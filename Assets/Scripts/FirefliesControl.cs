using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FirefliesControl : MonoBehaviour
{
    public GameObject fireflyPrefab;
    private ParticleSystem[][] _fireflies;
    private readonly List<ParticleSystem> _currentlyActiveFireflies = new();
    private GameObject _player;
    private readonly Vector2 _mapSize = new Vector2Int(400, 275);
    private readonly Vector3 _fireflySize = new Vector3Int(20, 4, 20);
    private int _hour;
    
    private List<GameObject> _objectsInWay = new();
    
    private void Start()
    {
        DaylightClock.OnTimeChange += OnTimeUpdated;
        _player = GameObject.Find("Player");
        if (_player == null)
        {
            throw new Exception("Player not found");
        }
        
        if(fireflyPrefab == null)
        {
            throw new Exception("Firefly prefab not set");
        }
        
        // generate all fireflies chunks
        var xFirefliesCount = (int)(_mapSize.x / _fireflySize.x) + 1;
        var yFirefliesCount = (int)(_mapSize.y / _fireflySize.z) + 1;
        _fireflies = new ParticleSystem[xFirefliesCount][];
        for (var i = 0; 10 + i * _fireflySize.x < _mapSize.x; i++)
        {
            _fireflies[i] = new ParticleSystem[yFirefliesCount];
            for (var j = 0; 10 + j * _fireflySize.z < _mapSize.y; j++)
            {
                var firefly = Instantiate(fireflyPrefab, transform);
                firefly.name = "Firefly " + i + " / " + j;
                firefly.transform.position = new Vector3(10 + i * _fireflySize.x, _fireflySize.y, 10 + j * _fireflySize.z);
                firefly.SetActive(false);
                var fireflyParticleSystem = firefly.GetComponent<ParticleSystem>();
                _fireflies[i][j] = fireflyParticleSystem;
            }
        }
        
        // start a coroutine that enable firefly chunk around the player
        StartCoroutine(FirefliesCoroutine());
    }

    private void OnTimeUpdated(int hour, int minute)
    {
        _hour = hour;
    }


    private List<ParticleSystem> GetFirefliesAround(Vector2Int chunkPosition)
    {
        var fireflies = new List<ParticleSystem>();
        for(var i = -1; i <= 1; i++)
        {
            for (var j = -1; j <= 1; j++)
            {
                var lookupChunk = chunkPosition + new Vector2Int(i, j);
                if(lookupChunk.x < 0 || lookupChunk.y < 0) continue;
                if(lookupChunk.x >= _fireflies.Length || lookupChunk.y >= _fireflies.Length) continue;
                fireflies.Add(_fireflies[lookupChunk.x][lookupChunk.y]);
            }
        }
        return fireflies;
    }

    private IEnumerator FirefliesCoroutine()
    {
        // run forever
        while (true)
        {
            var position = _player.transform.position;

            // if it's day time, disable all fireflies
            var isNightTime = _hour is > 20 or < 6;
            if (isNightTime)
            {
                // deactivate out of reach fireflies
                foreach (var firefly in from firefly in _currentlyActiveFireflies
                         let distance = Vector3.Magnitude(firefly.transform.position - position)
                         where distance > _fireflySize.x * 3
                         select firefly)
                {
                    firefly.Stop();
                    firefly.gameObject.SetActive(false);
                }

                var currentChunkPosition = new Vector2Int(
                    (int) (position.x / _fireflySize.x),
                    (int)(position.z / _fireflySize.z)
                );
                
                // activate fireflies in reach
                foreach (var firefly in GetFirefliesAround(currentChunkPosition))
                {
                    firefly.gameObject.SetActive(true);
                    firefly.Play();
                    _currentlyActiveFireflies.Add(firefly);
                }
            }
            else
            {
                // it's daytime, disable all enabled fireflies
                foreach (var firefly in _currentlyActiveFireflies)
                {
                    firefly.Stop();
                    firefly.gameObject.SetActive(false);
                }

                if (_currentlyActiveFireflies.Count > 0) _currentlyActiveFireflies.Clear();
            }
            
            yield return new WaitForSeconds(2);
        }
    }
}
