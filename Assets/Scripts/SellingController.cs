using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UIElements;

public class SellingController : MonoBehaviour
{
    private GameObject _player;

    private Inventory _inventory;

    private VisualElement _hudAction;

    private bool _isInCollider;

    [SerializeField] private FishType fishType;

    // Start is called before the first frame update
    private void Start()
    {
        _player = GameObject.Find("Player");
        _inventory = _player.GetComponent<Inventory>();
        _hudAction = GameObject.FindGameObjectWithTag("HUD_Action")
            .GetComponent<UIDocument>().rootVisualElement;
    }

    // Update is called once per frame
    private void Update()
    {
        if (!_isInCollider || (!Input.GetKeyDown(KeyCode.Space) && !Input.GetKeyDown(KeyCode.JoystickButton0))) return;
        // Sell all fishes of the specified type
        var quantity = _inventory.GetFishQuantity(fishType);
        if (quantity == 0) return;
        _inventory.SellFish(fishType, quantity);
        _hudAction.style.display = DisplayStyle.None;
    }

    private void OnTriggerEnter(Collider other)
    {
        _isInCollider = true;
        var fishName = fishType.NamePlural();
        var quantity = _inventory.GetFishQuantity(fishType);
        var totalPrice = fishType.Price() * quantity;
        if (quantity == 0)
        {
            _hudAction.Q<Label>("GameName").text = "Sell Fish";
            _hudAction.Q<Label>("Sentence").text = "Sorry, you haven't the fish I want.\nCome back when you have " + fishName; // Sentence for no action

            _hudAction.Q<VisualElement>("NoAction").style.display = DisplayStyle.Flex;
            _hudAction.Q<VisualElement>("Action").style.display = DisplayStyle.None;
        }
        else
        {
            _hudAction.Q<Label>("GameName").text = "Sell Fish";
            _hudAction.Q<Label>("Key").text = "\"" + KeyCode.Space.ToString() + "\"";
            _hudAction.Q<Label>("For").text = "to sell all " + fishName + "\nfor " + totalPrice + " $"; // sentence for action

            _hudAction.Q<VisualElement>("NoAction").style.display = DisplayStyle.None;
            _hudAction.Q<VisualElement>("Action").style.display = DisplayStyle.Flex;
        }

        _hudAction.style.display = DisplayStyle.Flex;
    }

    private void OnTriggerExit(Collider other)
    {
        _isInCollider = false;
        _hudAction.style.display = DisplayStyle.None;
    }
}
