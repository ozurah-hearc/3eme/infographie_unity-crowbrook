using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using static System.Runtime.CompilerServices.RuntimeHelpers;

[RequireComponent(typeof(FishingGameController))]
public class FishSpotColliderController : MonoBehaviour
{
    private FishingGameController _fishGameController;
    private VisualElement _hudAction;

    private bool _isPlayerInCollider = false;
    private bool _isPlayerMoving = false;

    [SerializeField] private string gameName = "Fishing Game";
    [SerializeField] private KeyCode startGameKey = KeyCode.P;

    private GameObject player;

    // Start is called before the first frame update
    private void Start()
    {
        player = GameObject.Find("Player");
        if (player == null)
            throw new System.Exception("Player not found");

        _fishGameController = GetComponent<FishingGameController>();
        _hudAction = GameObject.FindGameObjectWithTag("HUD_Action")
            .GetComponent<UIDocument>().rootVisualElement;
        if (_hudAction == null)
            throw new System.Exception("HUD_Action not found");

        MoveToClickPoint.OnStartMoving += () => OnPlayerMoveChange(true);
        MoveToClickPoint.OnStopMoving += () => OnPlayerMoveChange(false);

        _fishGameController.OnGameEnd += FishingGameController_OnGameEnd;
    }

    // Update is called once per frame
    private void Update()
    {
        if (!_fishGameController.CanFishing || _fishGameController.IsPlaying ||
            (!Input.GetKeyDown(startGameKey) && !Input.GetKey(KeyCode.JoystickButton0))) return;

        _hudAction.style.display = DisplayStyle.None;
        _fishGameController.StartGame();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.gameObject.CompareTag("Player")) return;
        _isPlayerInCollider = true;
        
        ApplyStartableGame(CheckCanStartGame());
    }

    private void OnPlayerMoveChange(bool isMoving)
    {
        if (!_isPlayerInCollider) return;
        
        _isPlayerMoving = isMoving;

        ApplyStartableGame(CheckCanStartGame());
    }

    private bool CheckCanStartGame()
    {
        return !_isPlayerMoving && _isPlayerInCollider && CheckAngle(player, gameObject);
    }

    private void ApplyStartableGame(bool startable)
    {
        if (startable)
        {
            _fishGameController.CanFishing = true;

            _hudAction.Q<Label>("GameName").text = gameName;
            _hudAction.Q<Label>("Key").text = "\"" + startGameKey.ToString() + "\"";
            _hudAction.Q<Label>("For").text = "to start the game";
            _hudAction.Q<VisualElement>("NoAction").style.display = DisplayStyle.None;
            _hudAction.Q<VisualElement>("Action").style.display = DisplayStyle.Flex;

            _hudAction.style.display = DisplayStyle.Flex;
        }
        else
        {
            _fishGameController.CanFishing = false;
            _hudAction.style.display = DisplayStyle.None;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.gameObject.CompareTag("Player")) return;
        _isPlayerInCollider = false;

        ApplyStartableGame(false);
    }

    private void FishingGameController_OnGameEnd(FishingGameController sender, GameStopType stopType)
    {
        ApplyStartableGame(CheckCanStartGame());
    }

    private static bool CheckAngle(GameObject player, GameObject pond)
    {
        var direction = pond.transform.position - player.transform.position;
        return Vector3.Angle(direction, player.transform.forward) is > -90 and < 90;
    }
}
