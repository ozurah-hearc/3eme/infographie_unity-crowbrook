using System;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
public class MoveToClickPoint : MonoBehaviour
{
    public static event Action OnStartMoving;
    public static event Action OnStopMoving;

    public bool CanMove { get; set; } = true;

    private static readonly int IsMoving = Animator.StringToHash("IsMoving");

    private NavMeshAgent _agent;
    private Animator _animator;
    private Camera _camera;

    private void Start()
    {
        _agent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
        _camera = Camera.main;

        _agent.autoBraking = false;
        _agent.speed = 7;
    }

    private void Update()
    {
        if (!CanMove)
        {
            _animator.SetBool(IsMoving, false);
            return;
        }

        if (Input.GetMouseButton(0))
        {
            if (Physics.Raycast(_camera.ScreenPointToRay(Input.mousePosition), out var hit, 100, LayerMask.GetMask("Walkable")))
            {
                _animator.SetBool(IsMoving, true);
                _agent.SetDestination(hit.point);

                OnStartMoving?.Invoke();
            }
        }


        // handle xbox controller
        if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0)
        {
            _animator.SetBool(IsMoving, true);
            _agent.SetDestination(transform.position + new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical")));

            OnStartMoving?.Invoke();
        }

        if (Vector3.Distance(_agent.destination, transform.position) == 0)
        {
            _animator.SetBool(IsMoving, false);

            OnStopMoving?.Invoke();
        }

    }
}