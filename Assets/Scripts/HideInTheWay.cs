using System.Collections.Generic;
using UnityEngine;
using System;

public class HideInTheWay : MonoBehaviour
{
    private static readonly int CutoutPos = Shader.PropertyToID("_Cutout_Pos");
    private static readonly int CutoutSize = Shader.PropertyToID("_Cutout_Size");
    private static readonly int FalloffSize = Shader.PropertyToID("_Falloff_Size");
    
    private readonly List<GameObject> _objectsInWay = new();
    private readonly List<GameObject> _objectsTransparent = new();
    
    private Camera _camera;
    private GameObject _player;

    private void Start()
    {
        _camera = Camera.main;
        _player = GameObject.Find("Player");
        if(_player == null)
        {
            throw new Exception("Player not found");
        }
    }

    private void Update()
    {
        _objectsInWay.Clear();

        var hits = GetAllObjectsInTheWay();
        foreach(var hit in hits)
        {
            var hitCollider = hit.collider;
            if (hitCollider == null) continue;
            _objectsInWay.Add(hitCollider.gameObject);
        }

        foreach (var obj in _objectsInWay)
        {
            MakeTransparent(obj);
        }

        foreach (var obj in _objectsTransparent)
        {
            MakeSolid(obj);
        }

        _objectsTransparent.RemoveAll(obj => !_objectsInWay.Contains(obj));

    }

    private IEnumerable<RaycastHit> GetAllObjectsInTheWay()
    {
        var position = transform.position;
        var playerPosition = _player.transform.position;
        var cameraPlayerDistance = Vector3.Magnitude(position - playerPosition);

        var results = new RaycastHit[20];
       
        var frontwardSize =  Physics.SphereCastNonAlloc(position, 0.1f, (playerPosition - position).normalized, results, cameraPlayerDistance, LayerMask.GetMask("Buildings"));
        var backwardSize = Physics.SphereCastNonAlloc(playerPosition, 0.1f, (position - playerPosition).normalized, results, cameraPlayerDistance, LayerMask.GetMask("Buildings"));

        var resultSize = frontwardSize + backwardSize;
        
        var hits = new RaycastHit[resultSize];
        Array.Copy(results, hits, resultSize);
        return hits;
    }
    
    private void MakeSolid(GameObject obj)
    {
        if (_objectsInWay.Contains(obj)) return;

        var materials = obj.GetComponent<Renderer>().materials;
        foreach (var material in materials)
        {
            material.SetFloat(CutoutSize, 0f);
        }
    }

    private void MakeTransparent(GameObject obj)
    {
        Vector2 cutoutPos = _camera.WorldToViewportPoint(_player.transform.position);
        cutoutPos.y /= ((float)Screen.width / Screen.height);
        var materials = obj.GetComponent<Renderer>().materials;
        foreach (var material in materials)
        {
            material.SetVector(CutoutPos, cutoutPos);
            material.SetFloat(CutoutSize, 0.1f);
            material.SetFloat(FalloffSize, 0.05f);
        }
        _objectsTransparent.Add(obj);
    }

}
