using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    /// <summary> Inventory = Sender, FishType = added fish, int = quantity </summary>
    public static event Action<Inventory, FishType, int> OnItemAdd;
    /// <summary> Inventory = Sender, FishType = added fish, int = quantity </summary>
    public static event Action<Inventory, FishType, int> OnItemRemove;

    /// <summary> Inventory = Sender, int = money </summary>
    public static event Action<Inventory, int> OnMoneyChange;

    // Number of fish in the inventory per fish type
    private readonly Hashtable _fishQuantityPerType = new Hashtable();

    // Money in the inventory
    private int _money = 0;
    
    public void AddFish(FishType fishType, int quantity)
    {
        // If the fish type is already in the inventory, add to the quantity
        if (_fishQuantityPerType.ContainsKey(fishType))
        {
            _fishQuantityPerType[fishType] = (int)_fishQuantityPerType[fishType] + quantity;
        }
        else
        {
            // Otherwise, add the fish type to the inventory
            _fishQuantityPerType.Add(fishType, quantity);
        }

        OnItemAdd?.Invoke(this, fishType, quantity);
    }

    public int GetFishQuantity(FishType fishType)
    {
        // If the fish type is in the inventory, return the quantity
        if (_fishQuantityPerType.ContainsKey(fishType))
        {
            return (int)_fishQuantityPerType[fishType];
        }
        return 0;
    }

    public Hashtable GetFishQuantityPerType()
    {
        return _fishQuantityPerType;
    }

    public void AddMoney(int amount)
    {
        _money += amount;
        OnMoneyChange?.Invoke(this, _money);
    }

    public int GetMoney()
    {
        return _money;
    }

    // Check if the inventory has enough fish to sell with the given quantity
    public bool CanSellFish(FishType fishType, int quantity)
    {
        if (_fishQuantityPerType.ContainsKey(fishType))
        {
            return (int)_fishQuantityPerType[fishType] >= quantity;
        }
        return false;
    }

    // Sell the fish from the inventory with the given quantity,
    // and return true if the trade was successful, false otherwise
    public bool SellFish(FishType fishType, int quantity)
    {
        // Check that we can sell the fish
        if (!CanSellFish(fishType, quantity))
        {
            return false;
        }

        // Subtract the quantity from the inventory
        var newQuantity = (int)_fishQuantityPerType[fishType] - quantity;
        if (newQuantity > 0)
        {
            _fishQuantityPerType[fishType] = newQuantity;
        }
        // If the quantity is 0, remove the fish type from the inventory
        else
        {
            _fishQuantityPerType.Remove(fishType);
        }

        // Add money to the inventory
        AddMoney(fishType.Price() * quantity);

        OnItemRemove?.Invoke(this, fishType, quantity);

        return true;
    }
}
