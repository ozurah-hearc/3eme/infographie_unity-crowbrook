﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public static class Utils
{
    public static void AddOrReplace<T>(this List<T> list, int index, T value)
    {
        if (list == null)
            return;

        if (list.Count > index)
            list[index] = value;
        else
            list.Add(value);
    }
}

