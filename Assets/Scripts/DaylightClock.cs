using System;
using UnityEngine;

public class DaylightClock : MonoBehaviour
{
    public static event Action<int, int> OnTimeChange;
    [Range(0, 24)]
    public float timeOfDay = 8;
    [Range(0, 360)]
    public float axisOffset = 45;
    public float orbitSpeed = 0.05f;
    public Light sun;
    public Gradient nightLight;
    public AnimationCurve sunCurve;
    
    private int _hour = 8;
    private int _minute = 59;

    private void OnValidate()
    {
        ProgressTime();
    }

    private void Update()
    {
        timeOfDay += Time.deltaTime * orbitSpeed;
        ProgressTime();
    }

    private void ProgressTime()
    {
        var currentTime = timeOfDay / 24;
        var sunRotation = Mathf.Lerp(-90, 270, currentTime);
        sun.transform.rotation = Quaternion.Euler(sunRotation, axisOffset, 0);
        timeOfDay %= 24;
        
        var newHour = Mathf.FloorToInt(timeOfDay);
        var newMinute = Mathf.FloorToInt((timeOfDay / (24f / 1440f) % 60));

        if (newHour != _hour || newMinute != _minute)
        {
            _hour = newHour;
            _minute = newMinute;
            OnTimeChange?.Invoke(_hour, _minute);
        }
        
        RenderSettings.ambientLight = nightLight.Evaluate(currentTime);
        sun.intensity = sunCurve.Evaluate(currentTime) * .7f;


    }
}
