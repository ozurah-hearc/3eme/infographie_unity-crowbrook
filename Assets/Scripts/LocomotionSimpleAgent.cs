using UnityEngine;
using UnityEngine.AI;

[RequireComponent (typeof (NavMeshAgent))]
[RequireComponent (typeof (Animator))]
public class LocomotionSimpleAgent : MonoBehaviour {
    private static readonly int IsMoving = Animator.StringToHash("IsMoving");
    private static readonly int Horizontal = Animator.StringToHash("Horizontal");
    private static readonly int Vertical = Animator.StringToHash("Vertical");
    
    private Vector2 _smoothDeltaPosition = Vector2.zero;
    private Vector2 _velocity = Vector2.zero;
    private Animator _animator;
    private NavMeshAgent _agent;

    private void Start ()
    {
        _animator = GetComponent<Animator> ();
        _agent = GetComponent<NavMeshAgent> ();
        _agent.updatePosition = false;
    }

    private void Update ()
    {
        var localTransform = this.transform;
        var worldDeltaPosition = _agent.nextPosition - localTransform.position;

        // Map 'worldDeltaPosition' to local space
        var dx = Vector3.Dot(localTransform.right, worldDeltaPosition);
        var dy = Vector3.Dot(localTransform.forward, worldDeltaPosition);
        var deltaPosition = new Vector2 (dx, dy);

        // Low-pass filter the deltaMove
        var smooth = Mathf.Min(1.0f, Time.deltaTime / 0.15f);
        _smoothDeltaPosition = Vector2.Lerp(_smoothDeltaPosition, deltaPosition, smooth);

        // Update velocity if time advances
        if (Time.deltaTime > 1e-5f)
            _velocity = _smoothDeltaPosition / Time.deltaTime;

        var shouldMove = _velocity.magnitude > 0.5f && _agent.remainingDistance > _agent.radius;

        // Update animation parameters
        _animator.SetBool(IsMoving, shouldMove);
        _animator.SetFloat(Horizontal, _velocity.x);
        _animator.SetFloat(Vertical, _velocity.y);
    }

    private void OnAnimatorMove ()
    {
        // Update position to agent position
        transform.position = _agent.nextPosition;
    }
}