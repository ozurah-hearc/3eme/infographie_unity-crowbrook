﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class Clouds : MonoBehaviour
{
    public List<GameObject> cloudPrefabs;
    public Vector2 mapSize = new(400, 250);
    public float padding = 50;
    [Range(0, 360)]
    public float windAngle = 45;
    [Range(0, 10)] public float windPower = 6;
    public float height = 50;
    
    private Vector3 _windDirection;

    
    public int cloudsCount = 10;
    
    private readonly List<GameObject> _clouds = new();

    private void Start()
    {
        GenerateClouds();
        _windDirection = new Vector3(windPower * Mathf.Sin(windAngle * Mathf.Deg2Rad), 0,
            windPower * Mathf.Cos(windAngle * Mathf.Deg2Rad));
    }

    private void Update()
    {
        foreach (var cloud in _clouds)
        {
            cloud.transform.position += _windDirection * Time.deltaTime;
            if (cloud.transform.position.x < -padding)
            {
                cloud.transform.position = new Vector3(mapSize.x + padding, height, cloud.transform.position.z);
            } else if (cloud.transform.position.x > mapSize.x + padding)
            {
                cloud.transform.position = new Vector3(-padding, height, cloud.transform.position.z);
            } else if (cloud.transform.position.z < -padding)
            {
                cloud.transform.position = new Vector3(cloud.transform.position.x, height, mapSize.y + padding);
            } else if (cloud.transform.position.z > mapSize.y + padding)
            {
                cloud.transform.position = new Vector3(cloud.transform.position.x, height, -padding);
            }
        }
    }

    private void OnDestroy()
    {
        while (_clouds.Count > 0) {
            Destroy(_clouds[^1].gameObject);
            _clouds.RemoveAt(_clouds.Count - 1);
        }
        
    }

    private void GenerateClouds()
    {
        for (var i = 0; i < cloudsCount; i++)
        {
            var randomCloudIndex = UnityEngine.Random.Range(0, cloudPrefabs.Count);
            var newCloud = Instantiate(cloudPrefabs[randomCloudIndex], transform);
            var x = UnityEngine.Random.Range(-padding, (int)mapSize.x + padding);
            var z = UnityEngine.Random.Range(-padding, (int)mapSize.y + padding);
            var angle = UnityEngine.Random.Range(0, 360);

            newCloud.transform.position = new Vector3(x, height, z);
            newCloud.transform.Rotate(Vector3.up, angle);
            
            _clouds.Add(newCloud);
        }
    }
}