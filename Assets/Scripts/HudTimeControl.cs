using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class HudTimeControl : MonoBehaviour
{
    private VisualElement _hudGlobal;
    private Label _uiTime;
    private VisualElement _uiClockHand;
    
    private void Start()
    {
        DaylightClock.OnTimeChange += OnTimeUpdated;

        _hudGlobal = GameObject.FindGameObjectWithTag("HUD_Global").GetComponent<UIDocument>().rootVisualElement;
        if (_hudGlobal == null)
            throw new System.Exception("HUD_Global not found");
        
        _uiTime = _hudGlobal.Q<Label>("time");
        if(_uiTime == null)
            throw new System.Exception("time not found");
        
        _uiClockHand = _hudGlobal.Q<VisualElement>("hand");
        if(_uiClockHand == null)
            throw new System.Exception("hand not found");
    }
    
    private void OnTimeUpdated(int hour, int minute)
    {
        _uiTime.text = hour.ToString("00") + ":" + minute.ToString("00");
        _uiClockHand.transform.rotation = Quaternion.Euler(0, 0, (minute / 60f) * 360f);
    }
}
