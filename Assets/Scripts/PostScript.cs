using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class PostScript : MonoBehaviour
{
    private void Start()
    {
        var action = GameObject.FindGameObjectWithTag("HUD_Action")
            .GetComponent<UIDocument>().rootVisualElement;
        if(action == null)
            throw new System.Exception("HUD_Action not found");
        
        action.style.display = DisplayStyle.None;

        var fishGame = GameObject.FindGameObjectWithTag("HUD_GameFish")
            .GetComponent<UIDocument>().rootVisualElement;
        if(fishGame == null)
            throw new System.Exception("HUD_GameFish not found");
        
        fishGame.style.display = DisplayStyle.None;
    }

}
