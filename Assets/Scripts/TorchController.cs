using UnityEngine;

public class TorchController : MonoBehaviour
{
    private Light _torchLight;
    private ParticleSystem _torchParticles;
    private bool _isOn = false;

    private void Start()
    {
        DaylightClock.OnTimeChange += OnTimeUpdated;

        _torchLight = transform.Find("Point Light").GetComponent<Light>();
        _torchParticles = transform.Find("Particle System").GetComponent<ParticleSystem>();
    }

    private void OnTimeUpdated(int hour, int minute)
    {
        var isNightTime = hour is >= 20 or < 6;
        switch (isNightTime)
        {
            case true when !_isOn:
                _torchLight.enabled = true;
                _torchParticles.Play();
                _isOn = true;
                break;
            case false when _isOn:
                _torchLight.enabled = false;
                _torchParticles.Stop();
                _isOn = false;
                break;
        }
    }
}
