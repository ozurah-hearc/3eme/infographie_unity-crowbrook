using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class FishingAnimatorManager : MonoBehaviour
{
    public event Action OnCastFinished;
    
    private static readonly int AnimStartFishing = Animator.StringToHash("StartFishing");
    private static readonly int AnimStopFishing = Animator.StringToHash("StopFishing");


    private Animator _animator;

    public void StartFishing()
    {
        _animator.SetTrigger(AnimStartFishing);
    }

    public void EndFishing()
    {
        _animator.SetTrigger(AnimStopFishing);
    }
    
    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnFinishCastAnimation()
    { // Called by event on the animator
        Debug.Log("OnFinishCastAnimation");
        OnCastFinished?.Invoke();
    }
}
