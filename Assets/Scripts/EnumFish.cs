public enum FishType
{
    AtlanticBass,
    Clownfish,
    BlueGill,
    GoldenTench,
}

public static class FishTypeExtension
{
    private static string SPRITE_RESOURCE_FOLDER = "Sprites/Fishes/";
    public static string Name(this FishType type)
    {
        return type switch
        {
            FishType.AtlanticBass => "Atlantic bass",
            FishType.Clownfish => "Clown fish",
            FishType.BlueGill => "Blue gill",
            FishType.GoldenTench => "Golden tench",
            _ => type.ToString(), // Enum value
        };
    }

    public static string NamePlural(this FishType type)
    {
        return type switch
        {
            FishType.AtlanticBass => "Atlantic basses",
            FishType.Clownfish => "Clown fishes",
            FishType.BlueGill => "Blue gills",
            FishType.GoldenTench => "Golden tenches",
            _ => type.ToString(), // Enum value
        };
    }

    public static string SpriteName(this FishType type)
    {
        return SPRITE_RESOURCE_FOLDER + type.ToString(); // Convention over configuration
        // Name of the sprite must be the same as the enum
    }

    public static int Price(this FishType type)
    {
        return type switch
        {
            FishType.AtlanticBass => 15,
            FishType.Clownfish => 20,
            FishType.BlueGill => 25,
            FishType.GoldenTench => 30,
            _ => 0, // Enum value
        };
    }
}
