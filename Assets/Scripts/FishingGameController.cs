using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.SocialPlatforms;
using UnityEngine.UIElements;
using Random = UnityEngine.Random;

public enum GameStopType { Win, Lose, Cancel }

public class FishingGameController : MonoBehaviour
{
    /// <summary> FishingGameController = sender </summary>
    public event Action<FishingGameController, GameStopType> OnGameEnd;


    private GameObject _player;
    private Inventory _inventory;

    private MoveToClickPoint _moveController; // To disable/enabled movement of the player
    private FishingAnimatorManager _fishingAnimatorManager;

    private VisualElement _hudGame;
    private VisualElement _gameCursor;
    private VisualElement _gameValidRange;
    private VisualElement _timer;
    private Label _uiKeyStopCursor;
    private Label _uiRemainingTime;
    private VisualElement _uiItemImage;
    private Label _uiItemName;
    private VisualElement _gameScreen;
    
    private VisualElement _winScreen;
    private Button _btnCloseWinScreen;

    private VisualElement _loseScreen;
    private Button _btnCloseLoseScreen;


    #region Game Config
    [SerializeField] private KeyCode stopCursorKey = KeyCode.Space;

    [SerializeField] private int nbRound = 3;
    [SerializeField] private float secondsBeforeNextRound = 2f;

    [SerializeField] private bool randomizeOnNewGame = true;

    [SerializeField] private FishType fishOnWin = FishType.Clownfish;

    [SerializeField] private List<float> sliderSpeed = new() { 1, 1.5f, 2 };
    [SerializeField] private List<float> sliderStartLocation = new() { 0 }; // 0 to 100

    [SerializeField] private List<float> startValidRange = new() { 0 }; // 0 to 100, < end // will force instead of random
    [SerializeField] private List<float> endValidRange = new() { 100 }; // 0 to 100, > start // will force instead of random
    
    [SerializeField] private float randomMinValidRange = 20f;
    [SerializeField] private List<float> randomValidRange = new() { 100 }; // 0 to 100 // The available range when random

    [SerializeField] private float randomSliderSpeedMin = 0.5f;
    [SerializeField] private float randomSliderSpeedMax = 2f;

    private float _remainingTimeBeforeNextRound = 0;
    private const float FrequencyRefreshRemainingTime = 1; // 1 = seconds, 10 = 0.1s, 100 = 0.01s, etc

    private int _currentRound = 1;
    private float _sliderLocation = 0;
    private float _currentSliderSpeed = 1;
    private float _currentStartValidRange = 0;
    private float _currentEndValidRange = 100;

    private int _sliderDirectionFactor = 1; // -1 = move to left / 1 = move to right
    private bool _isSliderMoving = false;

    private int _forcedSliderSpeed = 0;
    private int _forcedSliderStartLocation = 0;
    private int _forcedStartValidRange = 0;
    private int _forcedEndValidRange = 0;
    private int _forcedValidRange = 0;
    #endregion Game Config

    private bool _canFishing = false;
    /// <summary>
    /// Is the change of <see cref="_canFishing"/> was already detected and handled ?
    /// </summary>
    private bool _canFishingHandled = true;
    /// <summary>
    /// Is the player can start the fishing game.
    /// It also trigg the action to display the fishing game UI.
    /// </summary>
    public bool CanFishing
    {
        get => _canFishing;
        set
        {
            if (value != CanFishing)
                _canFishingHandled = false;

            _canFishing = value;

            OnCanFishingChange(GameStopType.Cancel);
        }
    }
    public bool IsPlaying { get; private set; } = false;

    private bool HasNextRound => _currentRound + 1 <= nbRound;

    private void OnValidate()
    {
        nbRound = Mathf.Clamp(nbRound, 1, 10);
        secondsBeforeNextRound = Mathf.Clamp(secondsBeforeNextRound, 0f, 15f);

        // random min valid range
        randomMinValidRange = Mathf.Clamp(randomMinValidRange, 1f, 100f);

        // Valid range
        for (var i = 0; i < randomValidRange.Count; i++)
        {
            randomValidRange[i] = Mathf.Clamp(randomValidRange[i], randomMinValidRange, 100f);
        }

        // Start valid range
        for (var i = 0; i < startValidRange.Count; i++)
        {
            var limit = 100f - 1f;
            if (endValidRange.Count > i)
                limit = endValidRange[i] - 1f;

            startValidRange[i] = Mathf.Clamp(startValidRange[i], 0, limit);
        }

        // End valid range
        for (var i = 0; i < endValidRange.Count; i++)
        {
            var limit = 0f + 1f;
            if (startValidRange.Count > i)
                limit = startValidRange[i] + 1f;

            endValidRange[i] = Mathf.Clamp(endValidRange[i], limit, 100f);
        }

        // Slider start location
        for (var i = 0; i < sliderStartLocation.Count; i++)
        {
            sliderStartLocation[i] = Mathf.Clamp(sliderStartLocation[i], 0f, 100f);
        }
    }

    // Start is called before the first frame update
    private void Start()
    {
        _player = GameObject.Find("Player");
        _inventory = _player.GetComponent<Inventory>();

        _moveController = _player.GetComponent<MoveToClickPoint>();
        _fishingAnimatorManager = _player.GetComponent<FishingAnimatorManager>();

        _hudGame = GameObject.FindGameObjectWithTag("HUD_GameFish")
            .GetComponent<UIDocument>().rootVisualElement;

        if (_hudGame == null)
            throw new Exception("HUD_GameFish not found");

        _uiKeyStopCursor = _hudGame.Q<Label>("keyStopCursor");

        _gameScreen = _hudGame.Q<VisualElement>("Game");
        _gameCursor = _hudGame.Q<VisualElement>("Cursor");
        _gameValidRange = _hudGame.Q<VisualElement>("ValidRange");

        _timer = _hudGame.Q<VisualElement>("Timer");
        _uiRemainingTime = _timer.Q<Label>("time");

        _winScreen = _hudGame.Q<VisualElement>("WinScreen");
        _uiItemImage = _winScreen.Q<VisualElement>("itemImage");
        _uiItemName = _winScreen.Q<Label>("itemName");
        _btnCloseWinScreen = _winScreen.Q<Button>("btnWinClose");

        _loseScreen = _hudGame.Q<VisualElement>("LoseScreen");
        _btnCloseLoseScreen = _loseScreen.Q<Button>("btnLoseClose");

        _forcedSliderSpeed = sliderSpeed.Count;
        _forcedSliderStartLocation = sliderStartLocation.Count;
        _forcedStartValidRange = startValidRange.Count;
        _forcedEndValidRange = endValidRange.Count;
        _forcedValidRange = randomValidRange.Count;

        AddRandomGameConfiguration();

        ConfigureGame(10, 30, 90, 1); // Setup some values, should not be used (usefull for UI debug)
    }

    // Update is called once per frame
    private void Update()
    {
        if (!IsPlaying)
            return;

        if (_isSliderMoving)
        {
            if (Input.GetKeyDown(stopCursorKey) || Input.GetKeyDown(KeyCode.JoystickButton0))
                FishingAction();

            MovingFishCursor();
        }
        else
        {
            DelayedStartNextRound();
        }
    }

    private void MovingFishCursor()
    {
        _sliderLocation += _currentSliderSpeed * _sliderDirectionFactor * (Time.deltaTime * 100);

        _sliderDirectionFactor = _sliderLocation switch
        {
            // Changing orientation
            >= 100 => -1,
            <= 0 => 1,
            _ => _sliderDirectionFactor
        };

        _sliderLocation = _sliderLocation switch
        {
            // Limit outbounds range
            < 0 => 0,
            > 100 => 100,
            _ => _sliderLocation
        };

        _gameCursor.style.left = Length.Percent(_sliderLocation - 50); // 0 to 100 ==>> -50 to 50, it's percent (0 == middle of the slider)
    }
    private void FishingAction()
    {
        if (_sliderLocation >= _currentStartValidRange && _sliderLocation <= _currentEndValidRange)
        {
            _isSliderMoving = false;
            Debug.Log("Nice, next round !");

            if (HasNextRound)
            {
                _remainingTimeBeforeNextRound = secondsBeforeNextRound;
                _uiRemainingTime.text = _remainingTimeBeforeNextRound.ToString("0");
                _timer.style.display = DisplayStyle.Flex;
            }
            else
            {
                Debug.Log("You win the game !");
                _uiItemName.text = fishOnWin.Name();

                _uiItemImage.style.backgroundImage =
                    new StyleBackground(Resources.Load<Texture2D>(fishOnWin.SpriteName()));

                DisplayScreen(false, true, false);

                _inventory.AddFish(fishOnWin, 1);
            }
        }
        else
        {
            // fishing fail
            Debug.Log("Fail, try again !");
            DisplayScreen(false, false, true);
        }
    }

    private void DisplayScreen(bool game, bool win, bool lose)
    {
        _gameScreen.style.display = game ? DisplayStyle.Flex : DisplayStyle.None;
        _winScreen.style.display = win ? DisplayStyle.Flex : DisplayStyle.None;
        _loseScreen.style.display = lose ? DisplayStyle.Flex : DisplayStyle.None;
    }

    private void DelayedStartNextRound()
    {
        if (_remainingTimeBeforeNextRound <= 0)
        {
            StartNextRound();
            return;
        }

        var dt = Time.deltaTime;
        var updateRemainingTime = (int)(_remainingTimeBeforeNextRound * FrequencyRefreshRemainingTime) != (int)((_remainingTimeBeforeNextRound - dt) * FrequencyRefreshRemainingTime);

        _remainingTimeBeforeNextRound -= dt;

        if (!updateRemainingTime) return;
        Debug.Log("Next round in : " + _remainingTimeBeforeNextRound);
        _uiRemainingTime.text = _remainingTimeBeforeNextRound.ToString("0");
    }

    /// <summary>
    /// Action when the <see cref="CanFishing"/> values changes
    /// </summary>
    private void OnCanFishingChange(GameStopType stopType)
    {
        if (_canFishingHandled)
            return;

        _canFishingHandled = true;

        switch (CanFishing)
        {
            case true:
                Debug.Log("Show 'Play fishing game'");
                break;
            case false:
                {
                    Debug.Log("Hide 'Play fishing game'");

                    if (IsPlaying)
                        StopGame(stopType);
                    break;
                }
        }
    }

    public void StartGame()
    {
        if (!CanFishing)
            return;

        // Assign the event for this game
        // note : if we assign the event in "start", we will have the event for each games
        _btnCloseWinScreen.clicked += StopGameWinDelegate;
        _btnCloseLoseScreen.clicked += StopGameLoseDelegate;
        _fishingAnimatorManager.OnCastFinished += OnCastFishingAnimEnd;

        DisplayScreen(false, false, false);
        
        _moveController.CanMove = false;

        _uiKeyStopCursor.text = stopCursorKey.ToString();

        _hudGame.style.display = DisplayStyle.Flex;

        if (randomizeOnNewGame)
            AddRandomGameConfiguration();

        _fishingAnimatorManager.StartFishing();
        
        // StartRound(1); The round start at the end of the animation
    }

    // Note 1 : We need to use a delegate to be able to remove lambda events
    // Note 2 : We need a lambda to pass arguments to the function
    private Action StopGameWinDelegate => () => StopGame(GameStopType.Win);
    private Action StopGameLoseDelegate => () => StopGame(GameStopType.Lose);

    private void StopGame(GameStopType stopType)
    {
        // Reset the event for this game
        _btnCloseWinScreen.clicked -= StopGameWinDelegate;
        _btnCloseLoseScreen.clicked -= StopGameLoseDelegate;
        _fishingAnimatorManager.OnCastFinished -= OnCastFishingAnimEnd;


        IsPlaying = false;
        _hudGame.style.display = DisplayStyle.None;

        _moveController.CanMove = true;

        _fishingAnimatorManager.EndFishing();
        
        OnGameEnd?.Invoke(this, stopType);
    }

    private void StartNextRound()
    {
        if (!HasNextRound)
            return;

        StartRound(_currentRound + 1);
    }

    private void StartRound(int roundNumber)
    {
        if (roundNumber < 1 || roundNumber > nbRound)
            throw new System.ArgumentOutOfRangeException(nameof(roundNumber), "The round number is out of range, it must be between 1 and " + nbRound);

        _timer.style.display = DisplayStyle.None;

        _currentRound = roundNumber;

        var indexRound = _currentRound - 1;

        ConfigureGame(
            sliderStartLocation[indexRound],
            startValidRange[indexRound],
            endValidRange[indexRound],
            sliderSpeed[indexRound]
            );

        ApplyUiGameConfiguration();

        _isSliderMoving = true;
    }

    private void AddRandomGameConfiguration()
    {
        fishOnWin = (FishType)Random.Range(0, Enum.GetNames(typeof(FishType)).Length);

        for (var i = _forcedSliderSpeed; i < nbRound; i++)
            sliderSpeed.AddOrReplace(i, Random.Range(randomSliderSpeedMin, randomSliderSpeedMax));

        for (var i = _forcedSliderStartLocation; i < nbRound; i++)
            sliderStartLocation.AddOrReplace(i, Random.Range(0, 100));

        // Note : Clamp to avoid outbounds values
        //        if forced value are in contradiction

        for (var i = _forcedValidRange; i < nbRound; i++)
        {
            float start = 0f;
            float end = 100f;

            if (startValidRange.Count > i)
                start = startValidRange[i] + 1f;
            if (endValidRange.Count > i)
                end = endValidRange[i] - 1f;

            var range = Random.Range(1f, end - start);
            randomValidRange.AddOrReplace(i, Mathf.Clamp(range, randomMinValidRange, 100f));
        }

        for (var i = _forcedStartValidRange; i < nbRound; i++)
        {
            if (endValidRange.Count - _forcedValidRange > i)
            {
                var range = endValidRange[i] - randomValidRange[i];
                startValidRange.AddOrReplace(i, Mathf.Clamp(range, 0f, 99f));
            }
            else
                startValidRange.AddOrReplace(i, Random.Range(0, 100 - randomValidRange[i]));
        }

        for (var i = _forcedEndValidRange; i < nbRound; i++)
        {
            var range = startValidRange[i] + randomValidRange[i];
            endValidRange.AddOrReplace(i, Mathf.Clamp(range, 1f, 100f));
        }
    }

    private void ConfigureGame(float wantedSliderLocation = 0, float startRange = 0, float endRange = 100, float speed = 1)
    {
        _sliderLocation = wantedSliderLocation;
        _currentStartValidRange = startRange;
        _currentEndValidRange = endRange;
        _currentSliderSpeed = speed;
    }

    private void ApplyUiGameConfiguration()
    {
        _gameCursor.style.left = Length.Percent(_sliderLocation - 50);
        _gameValidRange.style.left = Length.Percent(_currentStartValidRange);
        // Instead of using "left / right" it's simplier to use "left / width"
        _gameValidRange.style.width = Length.Percent(_currentEndValidRange - _currentStartValidRange);
    }

    private void OnCastFishingAnimEnd()
    {
        IsPlaying = true;
        DisplayScreen(true, false, false);
        StartRound(1);
    }
}
