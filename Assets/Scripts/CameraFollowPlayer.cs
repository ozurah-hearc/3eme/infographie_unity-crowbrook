using System;
using UnityEngine;

public class CameraFollowPlayer : MonoBehaviour
{
    private const float Distance = 25;
    private const int Angle = 40;
    private GameObject _player;

    private void Start()
    {
        _player = GameObject.Find("Player");
        if(_player == null)
        {
            throw new Exception("Player not found");
        }
    }

    private void LateUpdate()
    {
        FollowPlayer();
    }

    private void FollowPlayer()
    {
        var correctedPlayer = _player.transform.position + new Vector3(0, 2, 0);

        var targetPosition = new Vector3(
            correctedPlayer.x,
            correctedPlayer.y + Distance * Mathf.Cos(Angle * Mathf.Deg2Rad),
            correctedPlayer.z - Distance * Mathf.Sin(Angle * Mathf.Deg2Rad)
        );

        transform.position = targetPosition;
        transform.LookAt(correctedPlayer);
    }
}
