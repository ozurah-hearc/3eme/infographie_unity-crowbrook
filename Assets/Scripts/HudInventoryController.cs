using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class HudInventoryController : MonoBehaviour
{
    private VisualElement _hudInventory;
    private const int NB_FISH_SLOTS = 4;

    [SerializeField]
    private VisualTreeAsset slotTemplate;

    private VisualElement moneySlot;
    private Dictionary<FishType, VisualElement> fishSlotsMap;
    // Start is called before the first frame update
    void Start()
    {
        if (slotTemplate == null)
            throw new ArgumentNullException("Inventory slot template cannot be null");

        fishSlotsMap = new Dictionary<FishType, VisualElement>();

        // Get components
        _hudInventory = GameObject.FindGameObjectWithTag("HUD_Global")
            .GetComponent<UIDocument>().rootVisualElement
            ?.Q<VisualElement>("Inventory");

        if (_hudInventory == null)
            throw new Exception("Inventory of HUD_Global not found");

        VisualElement inventorySlots = _hudInventory.Q<VisualElement>("Slots")
            ?? throw new Exception("Slots of inventory not found");


        // Build inventory slots
        inventorySlots.Clear();

        moneySlot = GenerateSlot("Sprites/Dollar", "Cash", 0);
        inventorySlots.Add(moneySlot);

        for (int i = 0; i < NB_FISH_SLOTS; i++)
        {
            // TODO : For now we assume there is 1 fishtype per value (0 to NB_FISH_SLOTS)
            FishType fish = (FishType)i;

            VisualElement slot = GenerateSlot(fish.SpriteName(), fish.Name(), 0);

            fishSlotsMap.Add(fish, slot);

            inventorySlots.Add(slot);
        }

        // Add events
        Inventory.OnMoneyChange += Inventory_OnMoneyChange;
        Inventory.OnItemAdd += Inventory_OnItemChange;
        Inventory.OnItemRemove += Inventory_OnItemChange;
    }

    private VisualElement GenerateSlot(String spriteName, String itemName, int itemQuantity)
    {
        VisualElement slot = slotTemplate.Instantiate();

        VisualElement uiItemImage = slot.Q<VisualElement>("itemImage");
        Label uiItemName = slot.Q<Label>("itemName");
        Label uiItemQuantity = slot.Q<Label>("itemQuantity");

        uiItemImage.style.backgroundImage = new StyleBackground(Resources.Load<Texture2D>(spriteName));
        uiItemName.text = itemName;
        uiItemQuantity.text = itemQuantity.ToString();

        return slot;
    }

    private void Inventory_OnItemChange(Inventory sender, FishType fish, int quantity)
    {
        VisualElement element = fishSlotsMap[fish];
        int newQuantity = sender.GetFishQuantity(fish);

        element.Q<Label>("itemQuantity").text = newQuantity.ToString();
    }

    private void Inventory_OnMoneyChange(Inventory sender, int money)
    {
        moneySlot.Q<Label>("itemQuantity").text = money.ToString();
    }

    // Update is called once per frame
    void Update()
    {

    }
}
